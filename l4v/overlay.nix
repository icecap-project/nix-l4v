self: super: with self;

let
  pythonPackageOverrides = callPackage ./deps/python.nix {};

in {

  inherit (callPackage ./lib.nix {}) mkScopeWith;

  l4v = mkScopeWith (x: x.l4v or {}) (callPackage ./scope.nix {});

  biber = super.biber.overrideAttrs (attrs: {
    doCheck = false;
  });

  python2 = super.python2.override {
    packageOverrides = pythonPackageOverrides;
  };

  python3 = super.python3.override {
    packageOverrides = pythonPackageOverrides;
  };

}
