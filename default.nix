let
  pkgs = import ./nix-isabelle/nixpkgs {
    overlays = [
      (import ./nix-isabelle/isabelle/overlay.nix)
      (import ./l4v/overlay.nix)
    ];
    config = {
      allowUnfree = true;
      allowBroken = true;
      oraclejdk.accept_license = true;
    };
  };

in rec {
  inherit pkgs;
  inherit (pkgs) l4v;
  inherit (l4v) tests specs;
}
